import java.util.ArrayList;

import com.ing.blockchain.zk.dto.BoudotRangeProof;

public class InRangeProof {
	private ArrayList<Tuple> tuplesInRange;
	private BoudotRangeProof lowRangeProof;
	private BoudotRangeProof highRangeProof;
	public BoudotRangeProof getLowRangeProof() {
		return lowRangeProof;
	}
	public void setLowRangeProof(BoudotRangeProof lowRangeProof) {
		this.lowRangeProof = lowRangeProof;
	}
	public BoudotRangeProof getHighRangeProof() {
		return highRangeProof;
	}
	public void setHighRangeProof(BoudotRangeProof highRangeProof) {
		this.highRangeProof = highRangeProof;
	}
	public ArrayList<Tuple> getTuplesInRange() {
		return tuplesInRange;
	}
	public InRangeProof() {
		tuplesInRange = new ArrayList<Tuple>();
	}
}
