import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;

import com.ing.blockchain.zk.RangeProof;
import com.ing.blockchain.zk.dto.BoudotRangeProof;
import com.ing.blockchain.zk.dto.ClosedRange;

public class Querier {
	public static void main(String[]args) {
		Replier r = new Replier(true);
		Scanner s = new Scanner(System.in);
		boolean cont = true;
		while (cont) {
			System.out.println("Enter the range: a b");
			long low = s.nextLong();
			long high = s.nextLong();
			ClosedRange range = ClosedRange.of(BigInteger.valueOf(low), BigInteger.valueOf(high));
			InRangeProof proof = r.query(range);
			validate(proof, range);
			System.out.println("===================");
//			System.out.println("Continue? y/n");
//			cont = s.next().equals("y");
		}
		s.close();
	}
	public static void validate(InRangeProof proof, ClosedRange range) {		
		BoudotRangeProof lowProof = proof.getLowRangeProof();
		BoudotRangeProof highProof = proof.getHighRangeProof();
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance(SequencingLab.getDigestName());
		} catch (NoSuchAlgorithmException e) {
			System.out.println("No such algorithm: " + SequencingLab.getDigestName());
			e.printStackTrace();
		}
		ECPublicKeyParameters slPublicKey = SequencingLab.getPublicKey();
		ECDSASigner signer = new ECDSASigner(new HMacDSAKCalculator(new SHA256Digest()));
		signer.init(false, slPublicKey);
		
		// Checklist:
		// + Signatures are valid for all tuples
		// + Variations are correctly linked
		// More?
		// @TODO: Integer might not be enough as size for the tuples array
		long validationStartTime = System.nanoTime();
		for (int i = 0; i < proof.getTuplesInRange().size(); i++) {
			Tuple t = proof.getTuplesInRange().get(i);
			String tupleStr = t.toString();
			if (i + 1 < proof.getTuplesInRange().size() && 
					(t.getNextVariationCommitment().getVariation().getPosition() 
							!= proof.getTuplesInRange().get(i + 1).getVariationCommitment().getVariation().getPosition())) {
				System.err.println("Variations are not properly linked");
				System.exit(0);
			}
			md.update(tupleStr.getBytes());
			byte[] hash = md.digest();
			md.reset();
			boolean verified = signer.verifySignature(hash, t.getSignature()[0], t.getSignature()[1]);
			if (!verified) {
				System.err.println("Signature not verified!");
				System.exit(0);
			}
		}
		
		// @TODO: Make sure there are at least two tuples
		System.out.println("Number of tuples received on querier:" + proof.getTuplesInRange().size());
		System.out.println("First tuple first index: " + proof.getTuplesInRange().get(0).getPositionCommitment().getX());
		System.out.println("Last tuple last index: " + proof.getTuplesInRange().get(proof.getTuplesInRange().size() - 1).getNextPositionCommitment().getX());
		
		// First tuple in the list should have a current position that is less than the range start
		RangeProof.validateRangeProof(lowProof, proof.getTuplesInRange().get(0).getPositionCommitment().getCommitment(), ClosedRange.of(BigInteger.valueOf(0), range.getStart()));
		System.out.println("Low proof verified, commitment in range: 0, " + range.getStart());
		// Last tuple in the list should have a next position greater than the range end
		RangeProof.validateRangeProof(highProof, proof.getTuplesInRange().get(proof.getTuplesInRange().size() - 1).getNextPositionCommitment().getCommitment(), 
				ClosedRange.of(range.getEnd(), BigInteger.valueOf(SequencingLab.MAX_NUMBER_OF_BASES)));
		System.out.println("High proof verified, commitment in range: " + range.getEnd() + ", " + SequencingLab.MAX_NUMBER_OF_BASES);
		
		long validataionEndTime = System.nanoTime();
		System.out.println("Validation success! It took " + ((validataionEndTime - validationStartTime)) / 1000000 + " miliseconds");
	}
}
