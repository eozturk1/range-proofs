import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;

import com.ing.blockchain.zk.RangeProof;
import com.ing.blockchain.zk.dto.BoudotRangeProof;
import com.ing.blockchain.zk.dto.ClosedRange;
import com.ing.blockchain.zk.dto.SecretOrderGroup;
import com.ing.blockchain.zk.dto.TTPMessage;
import com.ing.blockchain.zk.util.InputUtils;

public class Replier {
	public static final boolean SAVE_TUPLES = true;
	
	ArrayList<Tuple> tuples;
	public Replier(boolean readFromFile) {
		SequencingLab sl = new SequencingLab();
		if (readFromFile) {
//			File variationsFile = new File("dna_variation_commitments.obj");
//			if (!variationsFile.exists()) {
//				System.out.println("Variations file does not exist. Aborting...");
//				System.exit(0);
//			}
//			
//			File positionsFile = new File("dna_variation_commitments.obj");
//			if (!variationsFile.exists()) {
//				System.out.println("Variations file does not exist. Aborting...");
//				System.exit(0);
//			}
//			
//			File signaturesFile = new File("signatures.obj");
//			if (!signaturesFile.exists()) {
//				System.out.println("Signatures file does not exist. Aborting...");
//				System.exit(0);
//			}
//			
//			File publicKeyFile = new File("public.key");
//			if (!publicKeyFile.exists()) {
//				System.out.println("Public key file does not exist. Aborting...");
//				System.exit(0);
//			}
//			File privateKeyFile = new File("private.key");
//			if (!privateKeyFile.exists()) {
//				System.out.println("Private key file does not exist. Aborting...");
//				System.exit(0);
//			}
			
			System.out.println("Reading variations from file.");
			tuples = new ArrayList<Tuple>();
			
			ArrayList<TTPMessage> pComs = (ArrayList<TTPMessage>)(ArrayList<?>) InputUtils.readObjects("dna_position_commitments.obj");
			System.out.println("Number of position commitments read from file: " + pComs.size());
			ArrayList<SimpleCommitment> vComs = (ArrayList<SimpleCommitment>)(ArrayList<?>) InputUtils.readObjects("dna_variation_commitments.obj");
			ArrayList<BigInteger[]> signatures =  (ArrayList<BigInteger[]>)(ArrayList<?>)InputUtils.readObjects("signatures.obj");
			SecretOrderGroup sog = (SecretOrderGroup) InputUtils.readObject("secret_order_group.obj");
			int i = 0;
			for (i = 0; i < pComs.size() - 1; i++) {
				pComs.get(i).getCommitment().setGroup(sog);
				Tuple t = new Tuple(pComs.get(i), vComs.get(i), vComs.get(i + 1), pComs.get(i + 1));
				t.setSignature(signatures.get(i));
				tuples.add(t);
			}
			
			pComs.get(i).getCommitment().setGroup(sog);
			
			System.out.println("Number of tuples read from file: " + tuples.size());
			
			
			
			// Domain params
	    	X9ECParameters ecp = SECNamedCurves.getByName("secp256r1");
		    ECDomainParameters domainParams = new ECDomainParameters(ecp.getCurve(),
		                                                             ecp.getG(), ecp.getN(), ecp.getH(),
		                                                             ecp.getSeed());
			
			
			// Recover public key
			byte[] publicBytes = InputUtils.readFileBytes("public.key");
			
			ECPublicKeyParameters publicKey = null;
			try {
				publicKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(publicBytes);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					    
			sl.setPublicKey(publicKey);
			
			
			// Recover private key
			BigInteger d = (BigInteger) InputUtils.readObject("private.key");
		
			
			ECPrivateKeyParameters privateKey = new ECPrivateKeyParameters(d, domainParams);
			sl.setPrivateKey(privateKey);

		} else {
			System.out.println("Sequencing...");
			tuples = sl.preprocess(SAVE_TUPLES);
		}
	}
	
	public InRangeProof query(ClosedRange range) {
		InRangeProof proof = new InRangeProof();
		long queryStartTime = System.nanoTime();
		for (Tuple t : tuples) {
			BigInteger currentPosition = BigInteger.valueOf(t.getVariationCommitment().getVariation().getPosition());
			BigInteger nextPosition = BigInteger.valueOf(t.getNextVariationCommitment().getVariation().getPosition());
			// Current and next are in range, reveal commitments, we can return the whole tuple here
			if (range.contains(currentPosition) && range.contains(nextPosition)) {
				proof.getTuplesInRange().add(t);
				continue;
			}
			boolean noSNPsExist = (currentPosition.compareTo(range.getStart()) == -1 && nextPosition.compareTo(range.getEnd()) == 1);
			if (noSNPsExist) {
				System.out.println("No SNPs exist in given range.");
			}
			// If the current position is out of range and next position is in range, we need to generate an "on the left" range proof
			// for the current position
			if ((!range.contains(currentPosition) && range.contains(nextPosition))
					|| noSNPsExist) {
				BoudotRangeProof lowRangeProof = RangeProof.calculateRangeProof(t.getPositionCommitment(), ClosedRange.of(BigInteger.valueOf(0), range.getStart()));
				proof.setLowRangeProof(lowRangeProof);
				proof.getTuplesInRange().add(t);
				// Move on to creating a high proof if there are no snps in given range
				if (!noSNPsExist)
					continue;
			}
			// If the current position is in range and next position is out of range, we need to generate an "on the right" range proof
			// for the next position
			if ((range.contains(currentPosition) && !range.contains(nextPosition))
					|| noSNPsExist) {
				BoudotRangeProof highRangeProof = RangeProof.calculateRangeProof(t.getNextPositionCommitment(), ClosedRange.of(range.getEnd(), BigInteger.valueOf(SequencingLab.MAX_NUMBER_OF_BASES)));
				proof.setHighRangeProof(highRangeProof);
				if (!noSNPsExist)
					proof.getTuplesInRange().add(t);
				break;
			}
		}
		
		if (proof.getTuplesInRange().size() == 0) {
			System.out.println("No tuples in range.");
		}
		
		long queryEndTime = System.nanoTime();
		System.out.println("Query result generation with filtering and two proof generation took: " + (queryEndTime - queryStartTime) / 1000000 + " miliseconds.");
		return proof;
	}
}
