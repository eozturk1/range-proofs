import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;

import com.ing.blockchain.zk.TTPGenerator;
import com.ing.blockchain.zk.components.SecretOrderGroupGenerator;
import com.ing.blockchain.zk.dto.SecretOrderGroup;
import com.ing.blockchain.zk.dto.TTPMessage;
import com.ing.blockchain.zk.util.InputUtils;

public class SequencingLab {
	private ECPrivateKeyParameters privateKey;
    private static ECPublicKeyParameters publicKey;	// public but private
    private SecretOrderGroup group;
	private MessageDigest md;
	private static String digestName = "SHA-256";

    // DNA related variables
	public static long MAX_NUMBER_OF_BASES = 3000000000L;
	public static long VARIATIONS_OCCUR_EVERY = 1000; // bases
	
	public SequencingLab() {
		// Generate public-private EC key pair
		generateECKeyPair();
		
		// Initialize the digest algorithm
		initializeDigest();
    }
	
	private void generateSecretGroup() {
		System.out.println("Secret group generation starting.");
		long secretGroupGenStart = System.nanoTime();
		group = new SecretOrderGroupGenerator().generate();
		long secretGroupGenFinish = System.nanoTime();
		CommitmentTester.displayTimeTaken(secretGroupGenStart, secretGroupGenFinish);
	}
	
	private void initializeDigest() {
		try {
			md = MessageDigest.getInstance(digestName);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("No such algorithm: " + digestName);
			e.printStackTrace();
		}
	}
	
	// Create signatures for tuples. 
	// Each tuple is in the form: Sig_{SL}\{COM(cur), \{COM2(V_i)\}, COM2(V_{i + 1}, COM(nxt))\} 
	// where $COM$ is a commitment scheme that allows zero-knowledge proof range queries, 
	// $COM2$ is a cheaper commitment scheme
	// $cur$ is the position of the variation $V_i$ on the DNA. 
	// $nxt$ is position of the next variation $V_{i + 1}$ on the DNA.
	// Variation positions for now can be deterministically uniformly distributed.
	// @TODO: Add -inf and +inf
	public ArrayList<Tuple> preprocess(boolean saveTuples) {
		// Generate a secret order group
		generateSecretGroup();
		
		// ArrayList for tuples
		ArrayList<Tuple> tuples = new ArrayList<Tuple>();
		
		// Commitment for the current position
		TTPMessage curPCom = null;
		// Commitment for the current variation
		SimpleCommitment curVCom = null;

		// Commitment for the next position
		TTPMessage nxtPCom = null;
		// Commitment for the next variation
		SimpleCommitment nxtVCom = null;
		
		long count = 0;
		long startTime = System.nanoTime();
		
		// Initialize signer with the private key
		ECDSASigner signer = new ECDSASigner(new HMacDSAKCalculator(new SHA256Digest()));
		signer.init(true, privateKey);
		
		System.out.println(MAX_NUMBER_OF_BASES);
		System.out.println("Starting the signing process.");
		for (long i = 0; i < MAX_NUMBER_OF_BASES; i += VARIATIONS_OCCUR_EVERY, count++) {
			long cur = i;
			long nxt = i + VARIATIONS_OCCUR_EVERY;
			// If this is the first run, create new current commitments
			if (nxtPCom == null && nxtVCom == null) {
				curPCom = TTPGenerator.generateTTPMessage(BigInteger.valueOf(cur), group);
				// \0 simply represents AA -- an SNP type.
				curVCom = new SimpleCommitment(new Variation(cur, '\0'));
			} else {	// Otherwise, next of the previous loop will be the current
				curPCom = nxtPCom;
				curVCom = nxtVCom;
			}
			// Measure how long a commit takes
			//long commitStartTime = System.nanoTime();
			nxtPCom = TTPGenerator.generateTTPMessage(BigInteger.valueOf(nxt), group);
			//long commitEndTime = System.nanoTime();
			// System.out.println("1 Commit took: " + (commitEndTime - commitStartTime) / 1000000 + " miliseconds.");

			// \0 simply represents AA -- an SNP type.
			nxtVCom = new SimpleCommitment(new Variation(nxt, '\0'));
			
			Tuple t = new Tuple(curPCom, curVCom, nxtVCom, nxtPCom);
			String strTuple = t.toString();
			
			//System.out.println("Tuple: " + strTuple);
			
			// Hash
			md.update(strTuple.getBytes());
			byte[] hash = md.digest();
			md.reset();
			
			// Sign
			long signatureStartTime = System.nanoTime();
			BigInteger[] signature = signer.generateSignature(hash);
//			System.out.println("Signature number of BigIntegers: " + signature.length);
//			System.out.println("Signature size: " + (signature[0].bitLength() + signature[1].bitLength()) + " bits.");
			long signatureEndTime = System.nanoTime();
			t.setSignature(signature);
			
//			// Add the tuple to return
//			tuples.add(t);
			
			// Instead of storing tuples in memory, we will write variations to a file
//			System.out.println("Saving position and variation commitment.");
			InputUtils.saveObject("dna_position_commitments.obj", curPCom, true);
			InputUtils.saveObject("dna_variation_commitments.obj", curVCom, true);
			
			InputUtils.saveObject("signatures.obj", signature, true);
			
			//System.out.println("Generated signature " + (++count) + " in " + (signatureEndTime - signatureStartTime) / 1000000 + " miliseconds.");
			if (count % 50000 == 0) {
				System.out.println("Run: " + count);
			}
		}
		
		// Also store last variations
//		System.out.println("Saving position and variation commitment.");
		InputUtils.saveObject("dna_position_commitments.obj", nxtPCom, true);
		InputUtils.saveObject("dna_variation_commitments.obj", nxtVCom, true);
		
		InputUtils.saveObject("secret_order_group.obj", nxtPCom.getCommitment().getGroup(), false);
		
		long endTime = System.nanoTime();
		long duration = (endTime - startTime);
		System.out.println("Time it took: " + (duration / 1000000) + " miliseconds.");
		
		if (saveTuples) {
			// InputUtils.saveObject("dna_tuples.obj", tuples, false);
			try {
				InputUtils.writeFileBytes("public.key", SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(publicKey).getEncoded());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputUtils.saveObject("private.key", privateKey.getD(), false);
		}
		
		return tuples;
	}
	
	
	private void generateECKeyPair() {
	    if (privateKey == null || publicKey == null) {
	    	X9ECParameters ecp = SECNamedCurves.getByName("secp256r1");
		    ECDomainParameters domainParams = new ECDomainParameters(ecp.getCurve(),
		                                                             ecp.getG(), ecp.getN(), ecp.getH(),
		                                                             ecp.getSeed());
		    // Generate a private key and a public key
		    AsymmetricCipherKeyPair keyPair;
		    ECKeyGenerationParameters keyGenParams = new ECKeyGenerationParameters(domainParams, new SecureRandom());
		    ECKeyPairGenerator generator = new ECKeyPairGenerator();
		    generator.init(keyGenParams);
		    keyPair = generator.generateKeyPair();

		    privateKey = (ECPrivateKeyParameters) keyPair.getPrivate();
		    publicKey = (ECPublicKeyParameters) keyPair.getPublic();
	    }
	}
	
	public static String getDigestName() {
		return digestName;
	}
	
	public SecretOrderGroup getSecretOrderGroup() {
		return group;
	}
	
	public static ECPublicKeyParameters getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(ECPublicKeyParameters publicKey) {
		this.publicKey = publicKey;
	}
	
	public void setPrivateKey(ECPrivateKeyParameters privateKey) {
		this.privateKey = privateKey;
	}
}
