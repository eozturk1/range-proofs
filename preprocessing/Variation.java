import java.io.Serializable;

public class Variation implements Serializable {
	private static final long serialVersionUID = -5674528094304609480L;
	private long position;
	// Different type of SNPs represented as a byte. 
	// [0,256] range of the byte can cover different possible SNP types ({A, T, C, G} x {A, T, C, G})
	// (size 16) as well as other mutations.
	private char snp;
	
	public Variation(long position, char snp) {
		this.position = position;
		this.snp = letter;
	}

	public long getPosition() {
		return position;
	}

	public char getSnp() {
		return snp;
	}
	
}
