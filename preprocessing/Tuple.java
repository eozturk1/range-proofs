import java.io.Serializable;
import java.math.BigInteger;

import com.ing.blockchain.zk.dto.TTPMessage;

public class Tuple implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6209886709104044578L;
	private TTPMessage positionCommitment;
	private SimpleCommitment variationCommitment;
	private SimpleCommitment nextVariationCommitment;
	private TTPMessage nextPositionCommitment;
	private BigInteger[] signature;
	
	public Tuple(TTPMessage positionCommitment, SimpleCommitment variationCommitment, 
			SimpleCommitment nextVariationCommitment, TTPMessage nextPositionCommitment) {
		this.positionCommitment = positionCommitment;
		this.variationCommitment = variationCommitment;
		this.nextVariationCommitment = nextVariationCommitment;
		this.nextPositionCommitment = nextPositionCommitment;
	}
	
	public void setSignature(BigInteger[] signature) {
		this.signature = signature;
	}

	public BigInteger[] getSignature() {
		return signature;
	}

	public TTPMessage getPositionCommitment() {
		return positionCommitment;
	}

	public SimpleCommitment getVariationCommitment() {
		return variationCommitment;
	}

	public SimpleCommitment getNextVariationCommitment() {
		return nextVariationCommitment;
	}

	public TTPMessage getNextPositionCommitment() {
		return nextPositionCommitment;
	}
	
	
	public String toString() {
		String strCurPCom = String.format("%064x", positionCommitment.getCommitment().getCommitmentValue());
		String strCurVCom = variationCommitment.getDigestHex();
		
		String strNxtPCom = String.format("%064x", nextPositionCommitment.getCommitment().getCommitmentValue());
		String strNxtVCom = nextVariationCommitment.getDigestHex();
		
		// Build and digest
		StringBuilder sb = new StringBuilder();
		sb.append(strCurPCom);
		sb.append('@');
		sb.append(strCurVCom);
		sb.append('@');
		sb.append(strNxtPCom);
		sb.append('@');
		sb.append(strNxtVCom);
		
		return sb.toString();
	};
}
