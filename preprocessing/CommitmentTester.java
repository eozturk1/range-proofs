import java.math.BigInteger;

import com.ing.blockchain.zk.RangeProof;
import com.ing.blockchain.zk.TTPGenerator;
import com.ing.blockchain.zk.components.SecretOrderGroupGenerator;
import com.ing.blockchain.zk.dto.BoudotRangeProof;
import com.ing.blockchain.zk.dto.ClosedRange;
import com.ing.blockchain.zk.dto.SecretOrderGroup;
import com.ing.blockchain.zk.dto.TTPMessage;

public class CommitmentTester {
	public static void main(String[] args) {
		long totalCommitmentTime = 0;
		long totalProofTime = 0;
		long totalValidationTime = 0;
		long totalSimpleCommitmentTime = 0;
		long totalSimpleCommitmentValidationTime = 0;
		int i = 0;
		System.out.println("Secret group generation starting.");
		long secretGroupGenStart = System.nanoTime();
		SecretOrderGroup group = new SecretOrderGroupGenerator().generate();
		long secretGroupGenFinish = System.nanoTime();
		displayTimeTaken(secretGroupGenStart, secretGroupGenFinish);
		System.out.println("");
		for (i = 0; i < 100; i++) {
			System.out.println("Starting commitment.");
			long commitStartTime = System.nanoTime();
			TTPMessage commitment = TTPGenerator.generateTTPMessage(BigInteger.valueOf(0), group);
			long commitFinishTime = System.nanoTime();
			totalCommitmentTime += displayTimeTaken(commitStartTime, commitFinishTime);
			
			ClosedRange range = ClosedRange.of(BigInteger.valueOf(-1), BigInteger.valueOf(5));
			
			System.out.println("Starting range proof.");
			long rangeProofStartTime = System.nanoTime();
			BoudotRangeProof proof = RangeProof.calculateRangeProof(commitment, range);
			long rangeProofFinishTime = System.nanoTime();
			totalProofTime += displayTimeTaken(rangeProofStartTime, rangeProofFinishTime);
			
			System.out.println("Validating proof.");
			long rangeValidationStartTime = System.nanoTime();
			RangeProof.validateRangeProof(proof, commitment.getCommitment(), range);
			long rangeValidationFinishTime = System.nanoTime();
			totalValidationTime += displayTimeTaken(rangeValidationStartTime, rangeValidationFinishTime);
			
			System.out.println("Starting simple commitment.");
			// \0 simply represents AA -- an SNP type.
			Variation v = new Variation(100000L, '\0');
			long simpleCommitmentStartTime = System.nanoTime();
			SimpleCommitment sc = new SimpleCommitment(v);
			long simpleCommitmentFinishTime = System.nanoTime();
			totalSimpleCommitmentTime += (simpleCommitmentFinishTime - simpleCommitmentStartTime);
			
			long simpleCommitmentValidataionStartTime = System.nanoTime();
			boolean validated = sc.validate();
			if (!validated) {
				System.err.println("Simple commitment not validated!");
			}
			totalSimpleCommitmentValidationTime += System.nanoTime() - simpleCommitmentValidataionStartTime;
		}
		System.out.println("\nAverage times in miliseconds...");
		System.out.println("Commitment time: " + totalCommitmentTime / i);
		System.out.println("Proof creation time: " + totalProofTime / i);
		System.out.println("Validation time: " + totalValidationTime / i);
		System.out.println("Simple commitment time in nanoseconds: " + totalSimpleCommitmentTime / i);
		System.out.println("Simple commitment validation time in nanoseconds: " + totalSimpleCommitmentValidationTime / i);
	}
	
	public static long displayTimeTaken(long operationStartTime, long operationFinishTime) {
		long timeTookInMiliseconds = (operationFinishTime - operationStartTime) /*/ 1000000*/;
		System.out.println("The operation took " + timeTookInMiliseconds + " miliseconds.");
		return timeTookInMiliseconds;
	}
}
