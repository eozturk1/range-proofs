import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

//import javax.xml.bind.DatatypeConverter;
import java.util.Base64;

public class SimpleCommitment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6790433565244026401L;
	public static int SALT_SIZE_BYTES = 16;
	private Variation variation;
	private BigInteger salt;
	private String digestHex;
	
	public SimpleCommitment(Variation variation) {
		this.variation = variation;	
		
		// Use a secure salt
		SecureRandom random = new SecureRandom();
		byte saltBytes[] = new byte[SALT_SIZE_BYTES];
		random.nextBytes(saltBytes);
		salt = new BigInteger(1, saltBytes);
		
		//System.out.println("Simple commitment, digest on: " + sb.toString());
		
		
		// Digest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("No such algorithm: SHA-256");
			e.printStackTrace();
		}
		md.update(getStringRepr(salt.toByteArray()).getBytes());
		byte[] digest = md.digest();
		
		this.digestHex = String.format("%064x", new BigInteger(1, digest));
	}
	
	public boolean validate() {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("No such algorithm: SHA-256");
			e.printStackTrace();
		}
		md.update(getStringRepr(salt.toByteArray()).getBytes());
		byte[] digest = md.digest();
		
		String calculatedDigest = String.format("%064x", new BigInteger(1, digest));
		return calculatedDigest.equals(digestHex);
	}
	
	private String getStringRepr(byte[] saltBytes) {
//		String saltHex = DatatypeConverter.printHexBinary(saltBytes);
		String saltHex = Base64.getEncoder().encodeToString(saltBytes);
		
		// Get string representation of the variation: position + '$' + letter + '$' + salt
		StringBuilder sb = new StringBuilder();
		sb.append(variation.getPosition());
		sb.append("$");
		sb.append(variation.getSnp());
		sb.append("$");
		sb.append(saltHex);
		
		return sb.toString();
	}

	public Variation getVariation() {
		return variation;
	}

	public BigInteger getRandom() {
		return salt;
	}

	public String getDigestHex() {
		return digestHex;
	}	
}
